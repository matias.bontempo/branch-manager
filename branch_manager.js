const $player = document.querySelector('#player');
const $choice = document.querySelector('#choice');
const $choice_timer = document.querySelector('#choice_timer');
const $choices = document.querySelector('#choices');

const choiceBuffer = 1;
const choiceFadeBuffer = 2;

const buffer = [];

let actualBranch;
let nextBranch;
let choicing = false;

const init = () => {
  $player.addEventListener("loadeddata", preloadBranches);
  $player.addEventListener('ended', playNextBranch);

  $player.addEventListener('timeupdate', playerTimeUpdate);

  setNextBranch('start');
  playNextBranch();
}

const preloadBranches = () => {
  console.log('Fully buffered');

  if (nextBranch.then) {
    preloadBranch(nextBranch.then);
    setNextBranch(nextBranch.then);
    return;
  };

  if (nextBranch.choice) {
    const { choices } = nextBranch.choice;
    $choices.innerHTML = '';
  
    choices.forEach(choice => {
      const e = document.createElement('li');
      e.onclick = () => setNextBranch(choice.path);
      e.innerHTML = choice.label;
      $choices.appendChild(e);
      if (buffer.indexOf(choice.path) === -1) preloadBranch(choice.path);
    });
  }
}

const preloadBranch = (branchName) => {
  console.log(`Preloading '${branchName}'`);
  buffer.push(branchName);
  const video = document.createElement('video');
  const branch = script[branchName];
  video.classList.add('preloading');
  video.preload = 'none';
  video.src = `./clips/${branch.clip}`;
  video.muted = true;
  video.play();
  document.body.appendChild(video);
  video.addEventListener('ended', () => videoPreloaded(video));
}

const videoPreloaded = (video) => {
  video.removeEventListener('ended', videoPreloaded);
  // document.body.removeChild(video);
}

const setNextBranch = (branchName) => {
  console.log(`Chosen '${branchName}'`);
  nextBranch = script[branchName];
}

const playNextBranch = () => {
  if ($player.src.indexOf(nextBranch.clip) !== -1) return;
  actualBranch = nextBranch;
  $player.src = `./clips/${nextBranch.clip}`;
  $player.play();
}

const playerTimeUpdate = () => {
  const { duration, currentTime } = player;
  const left = duration - currentTime;
  const { choice } = actualBranch;

  if (!duration) return;
  if (!choice) return;

  const choiceTime = choice.duration || 10;
  const startChoice = duration - choiceBuffer - choiceTime - choiceFadeBuffer*2;
  const endChoice = duration - choiceBuffer;

  if (currentTime >= startChoice && !choicing) {
    console.log('Choice START');
    $choice.classList.add('show');
    choicing = true;

  }
  if (currentTime >= endChoice && choicing) {
    console.log('Choice END');
    $choice.classList.remove('show');
    choicing = false;
  }

  if (choicing) {
    setChoicePercentage(((endChoice - choiceFadeBuffer) - currentTime) / choiceTime);
  }
}

const setChoicePercentage = p => $choice_timer.style.transform = `scaleX(${p > 0 ? p : 0})`;

init();
