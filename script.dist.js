const script = {
  "start": {
    "clip": "intro_choice.mp4",
    "choice": {
      "duration": '5',
      "choices": [
        {
          "label": "Sabor Original",
          "path": "sabor_original"
        },
        {
          "label": "Sabor Queso",
          "path": "sabor_queso"
        },
        {
          "label": "Sabor Cebolla",
          "path": "sabor_cebolla"
        }
      ]
    }
  },
  "sabor_cebolla": {
    "clip": "sabor_cebolla.mp4",
    "then": "ending"
  },
  "sabor_queso": {
    "clip": "sabor_queso.mp4",
    "then": "ending"
  },
  "sabor_original": {
    "clip": "sabor_original.mp4",
    "then": "ending"
  },
  "ending": {
    "clip": "ending.mp4",
    "choice" : {
      "duration": '3',
      "choices": [
        {
          "label": "Restart",
          "path": "start"
        },
        {
          "label": "Credits",
          "path": "credits"
        }
      ]
    }
  },
  "credits": {
    "clip": "credits.mp4",
  }
}